/* 
 */

let TOKEN;
let socket;
window.onload = function () {
    $('#registered-form').submit(submitForm);
    $('.main-container').addClass('visible');
    $('#registered-logout').click(logout);
    if (localStorage.token && localStorage.user) {
        getToken();
    }

    $('#chat-toggle-P').click(function () {
        $(this).toggleClass('active');
        $('.room-P').toggle();
    });
    $('#chat-toggle-F').click(function () {
        $(this).toggleClass('active');
        $('.room-F').toggle();
    });
    $('#chat-toggle-ID').click(function () {
        $(this).toggleClass('active');
        $('.room-ID').toggle();
    });
    $('#chat-toggle-IT').click(function () {
        $(this).toggleClass('active');
        $('.room-IT').toggle();
    });

    $('#chat-toggle-networking').click(function () {
        $(this).toggleClass('active');
        $('.room-networking').toggle();
    });

    $('#chat-toggle-Chevez').click(function () {
        $(this).toggleClass('active');
        $('.room-Chevez').toggle();
    });
    $('#chat-toggle-CME').click(function () {
        $(this).toggleClass('active');
        $('.room-CME').toggle();
    });
    $('#chat-toggle-Dentegra').click(function () {
        $(this).toggleClass('active');
        $('.room-Dentegra').toggle();
    });
    $('#chat-toggle-Centauro').click(function () {
        $(this).toggleClass('active');
        $('.room-Centauro').toggle();
    });
    $('#chat-toggle-BIVA').click(function () {
        $(this).toggleClass('active');
        $('.room-BIVA').toggle();
    });


};
let registeredData, dateString;

function getToken() {
    $('#chat-div').show();
    $('#chat-users-div').show();
    $('#registered-form').hide();

    $.post('/chat/getToken', {'user': localStorage.user, 'token': localStorage.token}).done(token => {
        TOKEN = token;
        startSocket();

    });
}
const messageOwner = {};
let chatMessages = [];

const sectionRename = {
    'P': 'Sesiones Plenarias',
    'F': 'Educación Financiera y Emprendimiento',
    'ID': 'Inclusión y Diversidad',
    'IT': 'Innovación y Tendencias',
    'Actinver': 'Odontoprev',
    'BIVA': 'Lo/Jack',
    'BMV': 'Google Cloud',
    'Chevez': 'PCS | Mexico',
    'CME': 'Charles Taylor InsureTech',
    'CREEL': 'El Asegurador',
    'Franklin': 'Milliman',
    'HR': 'gmmi'
};

function startSocket() {
    socket = io('https://socket.safetysummit2022.lat/chat');

    socket.on('connect', () => {
        socket.emit('requestAdmin', {
            token: TOKEN
        });
    });
    socket.on('adminchat', data => {
        messageOwner[data.id] = data.userId;
        $('#msg-' + data.id).addClass('msg-user-' + data.userId).attr('title', data.userId);
    });
    socket.on('chat', data => {
        const messageContainer = $('<div>').addClass('chat-message-container').attr('id', 'msg-' + data.id).appendTo('#chat-div');
        messageContainer.addClass('room-' + data.room);
        if (messageOwner[data.id]) {
            messageContainer.addClass('msg-user-' + messageOwner[data.id]).attr('title', messageOwner[data.id]);
        }
        const sender = $('<div>').addClass('chat-sender').appendTo(messageContainer);
        let sectionName = data.room;
        if (sectionRename[data.room]) {
            sectionName = sectionRename[data.room];
        }
        $('<span>').html(sectionName).addClass('chat-room').appendTo(sender);
        $('<span>').html(data.name).addClass('chat-name').appendTo(sender);
        $('<span>').html(data.enterprise).addClass('chat-enterprise').appendTo(sender);
        $('<span>').html(data.position).addClass('chat-position').appendTo(sender);

        const banBtn = $('<button>').addClass('ban-btn ban-btn-' + data.id).html('Bloquear').appendTo(sender);
        let banning = false;
        banBtn.click(() => {
            if (banning) {
                return false;
            }
            const ok = $('<button>').addClass('ok-btn ban-confirm').html('&#10004;').insertAfter(banBtn);
            const cancel = $('<button>').addClass('cancel-btn ban-confirm').html('&times;').insertAfter(banBtn);

            deleting = true;
            ok.click(() => {
                delBtn.remove();
                ok.remove();
                cancel.remove();
                socket.emit('ban', {
                    token: TOKEN,
                    user: messageOwner[data.id]
                });
                $('.msg-user-' + messageOwner[data.id]).addClass('banned');

                const forgiveDiv = $('<div>');
                const sender2 = $('<div>').addClass('chat-sender').appendTo(forgiveDiv);
                $('<span>').html(messageOwner[data.id] + ' ' + data.name).addClass('chat-name').appendTo(sender2);
                $('<span>').html(data.enterprise).addClass('chat-enterprise').appendTo(sender2);
                $('<span>').html(data.position).addClass('chat-position').appendTo(sender2);
                const forgiveBtn = $('<button>').addClass('forgive-btn').html('Perdonar').appendTo(sender2);
                $('#chat-users-div').append(forgiveDiv);
                forgiveDiv.click(() => {
                    forgiveDiv.remove();
                    $('.msg-user-' + messageOwner[data.id]).removeClass('banned');
                    socket.emit('forgive', {
                        token: TOKEN,
                        user: messageOwner[data.id]
                    });
                });

            });
            cancel.click(() => {
                ok.remove();
                cancel.remove();
                banning = false;
            });
        });
        let messageText = '';
        switch (data.message) {
            case ':r0:':
                messageText = '<img src="/img/sessions/reactions/0.png" alt="¡Bravo!">';
                break;
            case ':r1:':
                messageText = '<img src="/img/sessions/reactions/1.png" alt="Apretón">';
                break;
            case ':r2:':
                messageText = '<img src="/img/sessions/reactions/2.png" alt="OK">';
                break;
            case ':r3:':
                messageText = '<img src="/img/sessions/reactions/3.png" alt="Like">';
                break;
            default:
                messageText = data.message.trim();
        }
        const message = $('<div>').addClass('chat-message').html(messageText).appendTo(messageContainer);
        const delBtn = $('<button>').addClass('delete-btn').html('Eliminar').appendTo(message);
        let deleting = false;
        delBtn.click(() => {
            if (deleting) {
                return false;
            }
            const ok = $('<button>').addClass('ok-btn').html('&#10004;').insertAfter(delBtn);
            const cancel = $('<button>').addClass('cancel-btn').html('&times;').insertAfter(delBtn);

            deleting = true;
            ok.click(() => {
                delBtn.remove();
                ok.remove();
                cancel.remove();
                socket.emit('delete', {
                    token: TOKEN,
                    id: data.id
                });
            });
            cancel.click(() => {
                ok.remove();
                cancel.remove();
                deleting = false;
            });
        });

        $('#chat-div')[0].scrollTo(0, $('#chat-div')[0].scrollHeight);
        chatMessages.push(messageContainer);
        if (chatMessages.length > 64) {
            chatMessages.shift().remove();
        }
    });

    socket.on('delete', data => {
        $('#msg-' + data.id).addClass('deleted');
    });

    socket.on('banned', data => {
        for (let id in data) {
            if (!data[id]) {
                continue;
            }
            const forgiveDiv = $('<div>');
            const sender2 = $('<div>').addClass('chat-sender').appendTo(forgiveDiv);
            $('<span>').html(data[id] + ' ' + data.name).addClass('chat-name').appendTo(sender2);
            $('<span>').html(data.enterprise).addClass('chat-enterprise').appendTo(sender2);
            $('<span>').html(data.position).addClass('chat-position').appendTo(sender2);
            const forgiveBtn = $('<button>').addClass('forgive-btn').html('Perdonar').appendTo(sender2);
            $('#chat-users-div').append(forgiveDiv);
            forgiveDiv.click(() => {
                forgiveDiv.remove();
                $('.msg-user-' + data[id]).removeClass('banned');
                socket.emit('forgive', {
                    token: TOKEN,
                    user: data[id]
                });
            });

        }
    });



}

function submitForm(evt) {
    evt.preventDefault();
    localStorage.user = $('#registered-user').val();
    localStorage.token = $('#registered-password').val();
    getToken()
    return false;
}

function logout() {
    localStorage.user = false;
    localStorage.token = false;
    $('#registered-form').show();
    $('#chat-table tbody').empty();
    $('#chat-users-table tbody').empty();
    $('#chat-div').hide();
    $('#chat-users-div').hide();
}

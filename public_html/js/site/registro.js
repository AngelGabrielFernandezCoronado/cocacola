/* 
 */

/* global opera, site */



site.registro = {};
site.registro.onload = function (email) {
    $('.view-more').click(function () {
        if ($('#register-agreement-hidden').is(":visible")) {
            $('#register-agreement-hidden').show();
            $('.view-more').html('Ocultar ▴');
        } else {
            $('.view-more').html('Ver más ▾');
            $('#register-agreement-hidden').hide();
        }
    });

    obtener_localstorage();

function obtener_localstorage(){
    if( localStorage.getItem("mail")){
        $('#register-email').val(localStorage.getItem("mail"));
        $("#register-name").val(localStorage.getItem("name"));
        /*document.getElementById("register-email").val = localStorage.getItem("name");*/
        $("#register-company").val(localStorage.getItem("company"));
        $("#register-position").val(localStorage.getItem("position"));
 

    }else{
    console.log ("nada de nada");
    
}
}


    $('#home-icon').click(function () {
        location.href = 'https://www.convencionaseguradores.mx/';
    });

    $('.view-more').click(function () {
        $('#register-agreement-hidden').toggle();
        if ($('#register-agreement-hidden').is(":visible")) {
            $('.view-more').html('Ocultar ▴');
        } else {
            $('.view-more').html('Ver más ▾');
        }
    });

    $('#register-privacy-agree').click(function () {
        const input = $(this).find('input')[0];
        input.checked = !input.checked;
        if (input.checked) {
            $(this).addClass('active');
        } else {
            $(this).addClass('active');
        }
    });


    $('#register-privacy-agree-text').click(function () {
        $('#register-privacy-agree').click();
    });
    
    
    $('.register-radio').click(function () {
        $('.register-radio').removeClass('active');
        $('.register-radio input').prop('checked', false);
        $(this).addClass('active');
        const input = $(this).find('input').prop('checked', true);
    });
    $('#register-share-agree-text').click(function () {
        $('#register-share-agree').click();
    });
    
    $('#register-day23').click(function () {
        const input = $(this).find('input')[0];
        input.checked = !input.checked;
        if (input.checked) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
    
    $('#register-day23-text').click(function () {
        $('#register-day23').click();
    });
    
      $('#register-day24').click(function () {
        const input = $(this).find('input')[0];
        input.checked = !input.checked;
        if (input.checked) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
    
    $('#register-day24-text').click(function () {
        $('#register-day24').click();
    });
    
     $('#register-day25').click(function () {
        const input = $(this).find('input')[0];
        input.checked = !input.checked;
        if (input.checked) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
    
    $('#register-day25-text').click(function () {
        $('#register-day25').click();
    });
    
         $('#register-day26').click(function () {
        const input = $(this).find('input')[0];
        input.checked = !input.checked;
        if (input.checked) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
    
    $('#register-day26-text').click(function () {
        $('#register-day26').click();
    });
    
    $('#register-share-disagree-text').click(function () {
        $('#register-share-disagree').click();
    });

    
    
    $('#register-form input').prop('checked', false);

    $('#register-email').change(function () {
        $.post('/register/validate', {'email': $('#register-email').val()}, function (result) {
            if (result === 'valid') {
                $('#register-email-validate').html('');
            } else {
                $('#register-email-validate').html('');
                /*$('#register-email-validate').html('<br>Esta dirección de correo ya se encuentra registrada.');*/
            }
        });
    });


    $('#register-email2').change(function () {
        if ($('#register-email').val() === $('#register-email2').val()) {
            $('#register-email-confirm').html('');
        } else {
            $('#register-email-confirm').html('<br>Los correos electrónicos no coinciden');
        }
    });
    
    $('#register-sector').hide();
    
    $('#register-sector').change(function () {
        $('.register-conditioned').hide().prop('required', false);
        switch ($('#register-sector').val()) {
            case 'Pu':
                $('#register-company').show().val('').prop('required', true);
                $('#register-position').show().prop('required', true);
                $('#register-region').show().prop('required', true);
                break;
            case 'Pr':
                $('#register-company').show().val('').prop('required', true);
                $('#register-position').show().prop('required', true);
                $('#register-private').show().prop('required', true);
                $('#register-region').show().prop('required', true);
                break;
            case 'Ed':
                $('#register-university').show().prop('required', true);
                $('#register-edprofile').show().prop('required', true);
                break;
            case 'SC':
                $('#register-company').show().val('').prop('required', true);
                $('#register-position').show().prop('required', true);
                $('#register-region').show().prop('required', true);
                break;
            case '_':
                $('#register-company').show().val('').prop('required', true);
                $('#register-position').show().prop('required', true);
                $('#register-region').show().prop('required', true);
                break;
        }
    });


    $('.register-conditioned').show();
    $('#register-company-select').on('change', () => {
        if ($('#register-company-select').val().trim() === 'Otro') {
            $('#register-company').show();
            $('#register-company').val('').focus();
        } else {
            $('#register-company').val($('#register-company-select').val().trim());
            $('#register-company').hide();
        }
        const length = $('#register-company-select').val().trim().length;
        $('#register-company-select').css('width', (30 + length * 9.4) + 'px');

    });
     

    $('#register-company').on('change', () => {
        $('#register-company').val($('#register-company').val().trim());
    });

    $('#register-form').on('submit', site.registro.registerSubmit);

    if (email) {
        $('#register-email').val(email);
    }
};
site.registro.registerSubmit = function (evt) {
    evt.preventDefault();
    if ($('#register-email').val() !== $('#register-email2').val()) {
       /* $('#register-email-confirm').html('<br>Los correos electrónicos no coinciden');
        return false;*/
    }

   /* if($('#register-name').val()){

    }else{
        val('disabled',true);
    }*/

 /*  console.log($('#register-name').val());*/
   

   

    const registerData = {
        'agree': 'true',
        'email': $('#register-email').val(),
        'name': $('#register-name').val(),
        'lastName': 'NULL',
        'enterprise':$('#register-company').val(),
        'position':$('#register-position').val(),
        'phone':$('#register-phone').val(),
        'country':$("#register-region").val(),
        'registered': 1,
        'ws': 0

    };

  /*  switch ($('#register-sector').val()) {
        case 'Pu':
            registerData.enterprise = $('#register-company').val();
            registerData.position = $('#register-position').val();
            registerData.region = $('#register-region').val();
            break;
        case 'Pr':
            registerData.enterprise = $('#register-private').val() + ' | ' + $('#register-company').val();
            registerData.position = $('#register-position').val();
            registerData.region = $('#register-region').val();
            break;
        case 'Ed':
            registerData.enterprise = $('#register-university').val();
            registerData.position = $('#register-edprofile').val();
            break;
        case 'SC':
            registerData.enterprise = $('#register-company').val();
            registerData.position = $('#register-position').val();
            registerData.region = $('#register-region').val();
            break;
        case '_':
            registerData.enterprise = $('#register-company').val();
            registerData.position = $('#register-position').val();
            registerData.region = $('#register-region').val();
            break;
    }*/

    $.post('/register/validate', {'email': $('#register-email').val()}, (result) => {
        if (result === 'valid') {
            $('#register-form').css('pointer-events', 'none');
            $.post('/register', registerData).done(function (data) {
                $('#register-form')[0].reset();
                $('#register-checkbox').removeClass('active');
                $('body').addClass('success');
                $('#register-container').empty().load('/html/success.html', function () {
                    $('#reg-name').html(data.first_name);
                    $('#reg-email').html(data.email);
                    $('#reg-id').html(data.id);
                    $('#liga-id').html(data.id);
                    const url = 'https://safetysummit2022.lat/lobby/#/' + data.id;
                    $('#liga-a').attr('href', url);


                    $('#home-icon').click(function () {
                        location.href = 'https://www.convencionaseguradores.mx/';
                    });

                   /* $('#register-container').css('opacity', 100);
                    $('#liga-cal-google').attr('href', 'calendar/amis.ics?url=' + encodeURIComponent(url));
                    $('#liga-cal-ical').attr('href', 'calendar/amis.ics?url=' + encodeURIComponent(url));
                    $('#liga-cal-outlook').attr('href', 'calendar/amis.ics?url=' + encodeURIComponent(url)); 
                    fbq('track', 'CompleteRegistration');*/

                    $('#register-container').css('opacity', 100);
                    $('#liga-cal-google').attr('href', 'https://safetysummit2022.lat/calendar/amis.ics?url=' + encodeURIComponent(url));
                    $('#liga-cal-ical').attr('href', 'https://safetysummit2022.lat/calendar/amis.ics?url=' + encodeURIComponent(url));
                    $('#liga-cal-outlook').attr('href', 'https://safetysummit2022.lat/calendar/amis.ics?url=' + encodeURIComponent(url)); 
                    fbq('track', 'CompleteRegistration');
                });
            }).fail(function () {
                $('#register-button-container span').remove();
                $('#register-button-container').append('<span><br>Ha ocurrido un error, revise su conexión e intente más tarde.</span>');
            }).always(function () {
                $('#register-form').css('pointer-events', '');
            });

        } else {
            $('#register-button-container').append('<br>Esta dirección de correo ya se encuentra registrada.');
        }
    });
    return false;
};
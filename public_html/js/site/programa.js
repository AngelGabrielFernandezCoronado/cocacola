/* global site */

site.programa = {};
site.programa.onload = function () {
    $.post('/program/get',{},function(data){
        site.programa.data = data.data;
        site.programa.loadDay('2020-09-21');
    });
    $('.butonprogram1').click(function(){
        site.programa.loadDay('2020-09-21');
    });
    $('.butonprogram2').click(function(){
        site.programa.loadDay('2020-09-22');
    });
};

site.programa.loadDay= function(day){
    console.log(day);
    $('#program-table tbody').empty();
    for (const row of site.programa.data){
        if(row[0].substr(0,10) !== day){
            continue;
        }
       
       
       const tr = $('<tr>').appendTo('#program-table tbody').addClass('program-'+row[5]);
       const hourTd = $('<td>').appendTo(tr).html('<img src="img/reloj.png" class="program-clock" alt="">').append(row[1].substr(0,5));
       if(row[5] === 'panel'){
           hourTd.append('<span class="panel-span">Panel</span>');
       }
       const eventTd = $('<td>').appendTo(tr).addClass('program-event-td').html(row[3]);
    }
    $('#flex-button button').removeClass('active');
    $('.program-'+day).addClass('active');
};
    
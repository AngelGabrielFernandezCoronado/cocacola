/* global site */

site.savethedate = {};

site.savethedate.onload = function ()   {
    site.savethedate.refreshTime();
    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/") > -1) {
        $('.landing-counter-hexagon img').css('height', '80px');
    }

    $('#landing-enter').click(function () {
        const email = $('#landing-email').val();
        if (email.length < 3) {
            return false;
        }
        $.post('/user/getId', {'email': email}, function (result) {
            if (result === '0') {
                $('#error_message').append('<span><br>email not found, please contact support.</span>'); 
                return;
            }
            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/") > -1) {
                location.replace('microsoft-edge:https://' + window.location.host + '/event/#' + result);
                return;
            }
            location.href = 'event/#' + result;
        });
    });

    /*$('#landing-enter').click(function () {
        const email = $('#landing-email').val();
        if (email.length < 3) {
            return false;
        }
        $.post('/user/getId', {'email': email}, function (result)  {
          

            const usuario = {
                'id': result,
            };

            $.post('/userregistro', usuario).done(function (data)  {

                
            let mail = data.email;
            let name = data.first_name;
            let company = data.organization;
            let position = data.position;

            localStorage.setItem("mail", mail);
            console.log("el correo container localstorage " + mail);

            localStorage.setItem("name", name) ;
            console.log("el nombre container localstorage " + name);

            localStorage.setItem("company", company) ;
            console.log("el company container localstorage " + company);
            
            localStorage.setItem("position", position) ;
            console.log("el position container localstorage " + position);
            
            location.href = '#/registro'

                });
                if (result === '0') { 
                    $('#error_message span').remove();
                    $('#error_message').append('<span><br>email not found, please contact support.</span>');
                   return;
               }

        });
    });*/
};

  $('#landing-savethedate').empty().load('#/registro.html',  () => {
                    $('#reg-email').html(data.email);
                   /* console.log("el correo container es" + data.email);
                    localStorage.setItem("mail", data.email);
                   });*/

 
            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent) || navigator.userAgent.indexOf("Trident/") > -1) {
                location.replace('microsoft-edge:https://' + window.location.host + '/event/#' + result);
                return;
            }
            location.href = 'event/#' + result;
            
            /*const usuario = result;*/

            /*console.log(userId);*/
             /*      // (A) VARIABLES TO PASS
                    var first = "Foo Bar",
                        second = ["Hello", "World"];
                   
                    // (B) SAVE TO SESSION STORAGE
                    // sessionStorage.setItem("KEY", "VALUE");
                    sessionStorage.setItem("first", first);
                    // session storage cannot store array and objects
                    // JSON encode before storing, convert to string
                    sessionStorage.setItem("second", JSON.stringify(second));
                   
                    // (C) REDIRECT
                    location.href = "#/registro";
                    // Opening new window works too
                    // window.open("1b-session.html");*/
                  



                    $('#landing-savethedate').empty().load('html/success.html', () =>{
                    $('#reg-email').html(data.email);
                    
                   });
                    
            

       /* location.href = '#/registro' */
        
    const userId = result;

  

    });



site.savethedate.refreshTime = function () {
    const eventDate = new Date(Date.UTC(2020, 03, 21, 13, 30, 00));
    //'2020-09-21T08:30:00Z-06:00';
    const now = new Date();
    let delta = (eventDate.getTime() - now.getTime()) / 1000;


    if (delta <= 0) {
        $('#landing-counter').hide();
        $('#landing-registro').hide();
        $('#landing-enter').addClass('block');
        $('#landing-email').addClass('block');
        return;
    }

    if (delta <= 42700) {
        $('#landing-registro').hide();
        $('#landing-enter').addClass('block');
        $('#landing-email').addClass('block');
    }

// calculate (and subtract) whole days
    const days = Math.floor(delta / 86400);
    delta -= days * 86400;

// calculate (and subtract) whole hours
    const hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;

// calculate (and subtract) whole minutes
    const minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;

// what's left is seconds
    const seconds = Math.floor(delta % 60);  // in theory the modulus is not required

    //console.log(days, hours, minutes, seconds);
    document.getElementById('landing-counter-days').innerHTML = days.toString();
    document.getElementById('landing-counter-hours').innerHTML = hours.toString().padStart(2, "0");
    document.getElementById('landing-counter-minutes').innerHTML = minutes.toString().padStart(2, "0");
    document.getElementById('landing-counter-seconds').innerHTML = seconds.toString().padStart(2, "0");
    setTimeout(site.savethedate.refreshTime, 1000);


};

if (!String.prototype.padStart) { //Compatibility for Internet explorer
    String.prototype.padStart = function (targetLength, padString) {
        targetLength = targetLength >> 0; //truncate if number or convert non-number to 0;
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return String(this);
        } else {
            targetLength = targetLength - this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0, targetLength) + String(this);
        }
    };
}
/* 
 */
const sections = [ 'lobby', 'program', 'P', 'workshops', 'halloffame',
    'stands1', 'stands2', 'Showpoint', 'Inerco', 'Redtrack', 'Carstore',
   'Foundamentality', 'Freightliner', 'Cbip', 'Solistica', 'Alca',
   'Kaee', 'Zemog', 'Apex', 'Cesvi', 'Cepa', 'Virtuelle', 'Geotab',
   'Metrica', 'Blackay', 'Iiar', 'Draeger', 'Bosch', 'Dnv', 'Kroninn',
];
window.onload = function () {
    $('#registered-form').submit(submitForm);
    $('#registered-refresh').click(refresh);
    $('.main-container').addClass('visible');
    $('#registered-logout').click(logout);
    if (localStorage.token && localStorage.user) {
        refresh();
    }

    const sectionRename = {
        'P': 'Conferencias',
        /*'Risk': 'Risk',
        'SS&C': 'SSC',
        'Murex': 'Murex',
        'Mathworks':'Mathworks',
        'Matba Rofex': 'MatbaRofex',
        'AXIOM': 'Axiomsl',
        'GARP': 'GARP',
        'BITSO': 'Bitso',
        'BolsaSantiago': 'BolsaSantiago',
        'Calypso': 'Calypso'*/
    };
    $('#sections-tbody').empty();
    for (const section of sections) {
        const tr = $('<tr>').appendTo('#sections-tbody');
        let sectionName = section;
        if (sectionRename[section]) {
            sectionName = sectionRename[section];
        }
        $('<td>').html(sectionName).appendTo(tr);
        $('<td>').html('-').appendTo(tr).attr('id', '' + section + '-current');
        $('<td>').html('-').appendTo(tr).attr('id', '' + section + '-total');
        $('<td>').html('-').appendTo(tr).attr('id', '' + section + '-average');


    }
};
let registeredData, dateString;

function submitForm(evt) {
    evt.preventDefault();
    localStorage.user = $('#registered-user').val();
    localStorage.token = $('#registered-password').val();
    refresh();
    return false;
}

function logout() {
    localStorage.user = false;
    localStorage.token = false;
    $('#registered-form').show();
    $('#registered-container').hide();
    $('#registered-table tbody').empty();
}

function refresh() {
    $.post('/stats/get', {'user': localStorage.user, 'token': localStorage.token}, function (data) {
        $('#registered-form').hide();
        $('#registered-container').hide();
        console.log(data);

        $('#current-users').html(data.current_users);
        $('#total-users').html(data.total_users);



        $('#sections.tbody')
        let current_connections = 0;
        let total_connections = 0;
        for (const section of sections) {
            console.log($('#' + section + '-current'),data.current[section]);
            $('#' + section + '-current').html(data.current[section]);
            $('#' + section + '-total').html(data.total[section].visits);
            $('#' + section + '-average').html(Math.round(data.total[section].average / 60) + ' min');
            current_connections += Number(data.current[section]);
            total_connections += Number(data.total[section].visits);
        }


        $('#current-connections').html(current_connections);
        $('#total-connections').html(total_connections);

    }).fail(() => {
        $('#registered-form').show();
        $('#registered-container').hide();
    });
}


/* 
 */
const site = {};
$(document).ready(function () {
    $('#toggle').click(function () {
        $('#navbar').toggleClass('extended');
        if ($('#navbar').hasClass('extended')) {
            $('#navbar-links').addClass('extended');
        } else {
            $('#navbar-links').removeClass('extended');
        }
    });
});

let current = '';
window.onhashchange = function () {
    let id = window.location.hash.split('/')[1];
    let data = window.location.hash.split('/')[2];
    if (!index[id]) {
        id = 'savethedate';
    }
    const page = index[id];
    current = id;
    $('.main-container').removeClass('visible');
    $('#navbar-links a').removeClass('active');
    $('main').load('html/site/' + id + '.html', '', function () {
        const page = index[current];
        $('.site-' + current + '-link').addClass('active');
        if (page.title) {
            $('title').html(page.title + ' - Safety Summit');
        } else {
            $('title').html('Safety Summit');
        }
        if (page.dark) {
            $('body').addClass('dark');
        } else {
            $('body').removeClass('dark');
        }
        $('.main-container').addClass('visible');
        if (page.script) {
            site[current].onload(data);
        }
        parseLang();
    });
};

function changeLang() {
    if (lang === 'es') {
        lang = 'en';
    } else {
        lang = 'es';
    }
    parseLang();
}

function parseLang() {
    $('.lang').hide();
    $('.lang-' + lang).show();
    const inputs = $('.lang-placeholder');
   /* for (const input of inputs) {
        $(input).attr('placeholder', input.dataset['placeholderLang' + lang]);
    }*/

    const htmls = $('.lang-html');
  /*  for (const html of htmls) {
        $(html).html(html.dataset['htmlLang' + lang]);
    }*/

}


let lang = 'es';

userLang = navigator.language || navigator.userLanguage;

if (userLang.substr(0, 2).toLowerCase() !== 'es') {
  //  lang = 'en';  /* DISABLE LANGUAGE */
}

window.onload = function () {
    onhashchange();
};

$(document).ready(function () {
    $('#toggle').click(function () {
        $('#navbar').toggleClass('extended');
        if ($('#navbar').hasClass('extended')) {
            $('#navbar-links').addClass('extended');
        } else {
            $('#navbar-links').removeClass('extended');
        }

    });
    $('#navbar-logo').click(function () {
        location.href = '#/savethedate';
    });
    $('#lang-link').click(function () {
        changeLang();
    });
});



const index = {
    'savethedate':
    {
        title: 'savethedate',
        script: true
    },
    
    'registro':
            {
                title: 'Registro',
                script: true
            }
}
;
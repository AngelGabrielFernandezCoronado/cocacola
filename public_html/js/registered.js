/* 
 */


window.onload = function () {
    $('#registered-form').submit(submitForm);
    $('#registered-refresh').click(refresh);
    $('#registered-download').click(downloadCSV);
    $('.main-container').addClass('visible');
    $('#registered-logout').click(logout);
    if (localStorage.token && localStorage.user) {
        refresh();
    }
    $('#input-limit').on('change', refresh);
    $('#input-offset').on('change', refresh);
};
let registeredData, dateString;



function submitForm(evt) {
    evt.preventDefault();
    localStorage.user = $('#registered-user').val();
    localStorage.token = $('#registered-password').val();
    refresh();
    return false;
}

function logout() {
    localStorage.user = false;
    localStorage.token = false;
    $('#registered-form').show();
    $('#registered-container').hide();
    $('#registered-table tbody').empty();
}

function refresh() {
    $.post('/registered/count', {'user': localStorage.user, 'token': localStorage.token}).done(countdata => {
        $('#registered-form').hide();
        $('#registered-container').show();
        const max = countdata.COUNT;
        $('#registered-total').html(max);

        let limit = $('#input-limit').val();
        let offset = Number($('#input-offset').val()) - 1;
        if (limit > max) {
            $('#input-limit').val(max);
        }
        $('#input-limit').attr('max', max);
        if (offset * limit > max) {
            $('#input-offset').val((max / limit) + 1);
        }
        $('#input-offset').attr('max', (max / limit) + 1);

        limit = $('#input-limit').val();
        offset = Number($('#input-offset').val()) - 1;
        const page = offset * limit;

        $.post('/registered/list', {'user': localStorage.user,
            'token': localStorage.token,
            'limit': limit,
            'offset': page
        }).done(data => {
            $('#registered-form').hide();
            $('#registered-container').show();
            parseData(data);
        });


    }).fail(() => {
        $('#registered-form').show();
        $('#registered-container').hide();
        $('#registered-table tbody').empty();
        $('#registered-login-result').html('Credenciales no válidas');
    });
}

function parseData(data) {
    const date = new Date();
    dateString = date.getFullYear() + '-' + (Number(date.getMonth()) + 1) + '-' + date.getDay() + 'T' + date.getHours() + date.getMinutes() + date.getSeconds();
    console.log(data);
    $('#registered-table tbody').empty();

    registeredData = [];
    registeredData.push(data.header);
    setTimeout(() => {
        for (const row of data.data) {
            registeredData.push(row);
            const tr = $('<tr>').appendTo('#registered-table tbody');
            $('<td>').html(row[0]).appendTo(tr);
            $('<td>').html(row[1]).appendTo(tr);
            $('<td>').html(row[2]).appendTo(tr);
            $('<td>').html(row[3]).appendTo(tr);
            const sectors = {
                'Pr': 'Privado',
                'Pu': 'Público',
                'Ed': 'Educativo',
                'SC': 'Sociedad Civil',
                '_': 'Otro'
            };
            $('<td>').html(sectors[row[4]]).appendTo(tr);
            $('<td>').html(row[5]).appendTo(tr);
            $('<td>').html(row[6]).appendTo(tr);
            $('<td>').html(row[7]).appendTo(tr);
            $('<td>').html(row[8]).appendTo(tr);
            $('<td>').html(row[9]).appendTo(tr);
            $('<td>').html((Number(row[10]) === 1) ? 'No' : 'Si').appendTo(tr);
            $('<td>').html(row[11]).appendTo(tr);
        }
    }, 1000);
}

function downloadCSV() {
    $.post('/registered/csv', {'user': localStorage.user,
        'token': localStorage.token
    }).done(data => {
        const csvContent = data;
        const  textEncoder = new TextEncoder('windows-1252', {NONSTANDARD_allowLegacyEncoding: true});
        var csvContentEncoded = textEncoder.encode(csvContent);
        var blob = new Blob([csvContentEncoded], {type: 'text/csv;charset=windows-1252;'});
        saveAs(blob, "amis-registrados-" + dateString + ".csv");
    });

}
